
package Controlador;

import Clases.Usuario;
import Exception.BadPostCodeException;
import dao.gestorUsuario;
import funciones.herramientaEncriptar;
import funciones.validacionUsuario;

/**
 * @author Juan Quiros
 */
public class controlador {
    private Usuario usuarioLogueado=null; //Variable que mantiene al usuario logueado
    
    private validacionUsuario herramientaValUsuario = new validacionUsuario();
    private herramientaEncriptar herramientaEncrip = new herramientaEncriptar();
    private gestorUsuario gestor = new gestorUsuario();    

    
    //----------------Persistencia Datos
  
    public Usuario obtenerUsuario(int dni){
        Usuario user=this.gestor.obtenerUsuario(dni);
        return user;
    }
    public void insertarNuevo(Usuario usr_new){
        this.gestor.insert(usr_new);
    }
    public void actualizarUsuario(Usuario usr_){
        this.gestor.update(usr_);
    } 
    public int getCantidadUsuarios(){
        return this.gestor.obtenerUsuarios().size();
    }
    //----------------FIN Persistencia Datos
    
    //-----Registrar
    public void registrarNuevoUsuario(String nombre_, int dni_,String password1, String password2) throws BadPostCodeException{
        if(obtenerUsuario(dni_)==null){
            this.herramientaValUsuario.validarDni(dni_);
            this.herramientaValUsuario.validarNombre(nombre_);
            this.herramientaValUsuario.validarPassword(password1,password2);
            insertarNuevo(new Usuario(nombre_,dni_,this.herramientaEncrip.encriptar(password1),true));
        }else{
            throw new BadPostCodeException("El dni ya esta registrado");
        }
        
    }
    
    //-----Actualizar
    public void actualizarUsuarioLogueado(String nombre_, String password1, String password2) throws BadPostCodeException{
        if(this.usuarioLogueado!=null){
        if(nombre_.length()>0){
            this.herramientaValUsuario.validarNombre(nombre_);
            this.usuarioLogueado.setNombre(nombre_);
        }
        if(password1.length()>0){
            this.herramientaValUsuario.validarPassword(password1,password2);
            this.usuarioLogueado.setPassword(this.herramientaEncrip.encriptar(password2));
        }       
        actualizarUsuario(this.usuarioLogueado);
        }
    }
    
    
    
    //-----------controlador de sesion---------
    public Usuario loguearUsuario(int dni, String password_) throws BadPostCodeException{
       this.usuarioLogueado=obtenerUsuario(dni); 
        if(this.usuarioLogueado!=null && !herramientaEncrip.desencriptar(this.usuarioLogueado.getPassword()).equals(password_)){
            this.usuarioLogueado = null;
        }        
        return this.usuarioLogueado;
    }
    
    public void cerrarSesion(){
        this.usuarioLogueado = null;
    }

    public Usuario getUsuarioLogueado() {
        return usuarioLogueado;
    }
     
}
