
package dao;

import javax.persistence.criteria.CriteriaQuery;
import Clases.Usuario;
import java.util.ArrayList;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;
import org.hibernate.Session;
import org.hibernate.Transaction;


/**
 *
 * @author Juan Quiros
 */
public class gestorUsuario {
    
    public gestorUsuario(){
      //-------Abrir la session a utilizar durante el funcionamiento del programa
      HibernateUtil.getSessionFactory().openSession();
    }
       
    
    //---------Insertar nuevo Usuario      
    public void insert(Usuario u){ 
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            session.save(u);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
              transaction.rollback();
            }
            System.out.println(e.getMessage());
        }
    }
    //----------Actualizar
      public void update(Usuario u){
            Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
            Transaction tx = sesion.beginTransaction();                  
            sesion.update(u); 
            tx.commit();   
        }
    
    //---------obtener usuario por su dni
     public Usuario obtenerUsuario(int dni_) {   
        Usuario usuario = null; 
        Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = sesion.beginTransaction();
        CriteriaBuilder cb = sesion.getCriteriaBuilder();
        CriteriaQuery<Usuario> cq = cb.createQuery(Usuario.class);
        Root<Usuario> rootEntry = cq.from(Usuario.class);
        CriteriaQuery<Usuario> all = cq.select(rootEntry).where(cb.equal(rootEntry.get("dni"),dni_)); 
        TypedQuery<Usuario> allQuery = sesion.createQuery(all);        
        if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
                usuario = (Usuario) allQuery.getResultList().get(0);
            }
        tx.commit();
        return usuario ;       
    }
     
     //--------Obtiene un arrayList con todos los usuarios de la tabla mysql
     public ArrayList<Usuario> obtenerUsuarios() {
        ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
        Session sesion= HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = sesion.beginTransaction();
        CriteriaBuilder cb = sesion.getCriteriaBuilder();
        CriteriaQuery<Usuario> cq = cb.createQuery(Usuario.class);
        Root<Usuario> rootEntry = cq.from(Usuario.class);
        CriteriaQuery<Usuario> all = cq.select(rootEntry); 
        TypedQuery<Usuario> allQuery = sesion.createQuery(all);            
         if(allQuery.getResultList()!=null && !allQuery.getResultList().isEmpty()){
            usuarios = (ArrayList<Usuario>) allQuery.getResultList();
        }
        tx.commit();
        return usuarios;
    }
     
     
     
     
}
