package funciones;

import Exception.BadPostCodeException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 * @author Juan Quiros
 */
public class herramientaEncriptar {
    private String key = "Bar12345Bar12345"; // llave de 128 bit
    private String initVector = "RandomInitVector"; // 16 bytes   

   
    //toma una cadena y devuelve una encriptacion mesclando con la clave de 128Bit
     public String encriptar(String value) throws BadPostCodeException{
         byte[] encrypted = null;
         try{
            IvParameterSpec iv = new IvParameterSpec(this.initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(this.key.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            encrypted = cipher.doFinal(value.getBytes());
            } catch (NoSuchAlgorithmException| NoSuchPaddingException| InvalidKeyException| InvalidAlgorithmParameterException| IllegalBlockSizeException| BadPaddingException |UnsupportedEncodingException ex) {
                throw new BadPostCodeException( ex.getMessage());
            }
            return Base64.encodeBase64String(encrypted);       
    }

    //toma una cadena la cual pertenece a un password encriptado, lo desifra utilizando la llave de 128bit y devuelve la clave original
    public String desencriptar(String encrypted) throws BadPostCodeException  {        
            IvParameterSpec iv;
             byte[] original=null;
            try {
                iv = new IvParameterSpec(this.initVector.getBytes("UTF-8"));
                SecretKeySpec skeySpec = new SecretKeySpec(this.key.getBytes("UTF-8"), "AES");
                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
                cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
                original = cipher.doFinal(Base64.decodeBase64(encrypted));
            } catch (NoSuchAlgorithmException| NoSuchPaddingException| InvalidKeyException| InvalidAlgorithmParameterException| IllegalBlockSizeException| BadPaddingException |UnsupportedEncodingException ex) {
                throw new BadPostCodeException( ex.getMessage());
            }
            return new String(original);
       
    }
}
