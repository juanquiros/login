package funciones;

import Clases.Usuario;
import Exception.BadPostCodeException;

/**
 * @author Juan Quiros
 */
public class validacionUsuario {
        
    herramientaEncriptar encriptar = new herramientaEncriptar();
     public void validarPassword(String password_, String password2_) throws BadPostCodeException{     
        String mensaje="";      
        if(!password_.equals(password2_)){
             mensaje = "Error password, los password no coinciden\n";
        }
        if(password_.length() < 8 || password_.length() > 30){
           mensaje += "Error password, debe tener entre 8 y 30 caracteres\n";
        }        
        if ( ! password_.matches(".*[A-Z].*") ) { 
             mensaje +="El password debe tener por lo menos una MAYUSCULA\n";
        }
       if ( ! password_.matches(".*[a-z].*") ) { 
              mensaje +="El password debe tener por lo menos una minuscula\n";
        }
       if ( ! password_.matches(".*[0-9].*") ) { 
             mensaje +="El password debe tener por lo menos un numero\n";
        }
       if(mensaje.length()>0){
           throw new BadPostCodeException(mensaje); 
       }      
    }
    public boolean logearUsuario(Usuario udr,String password_) throws BadPostCodeException {
         boolean coinciden = false;         
         if(encriptar.desencriptar(udr.getPassword()).equals(password_)){
             coinciden = true;
         }
         return coinciden;
     }   
    public void validarNombre(String nom_) throws BadPostCodeException{
        if(nom_.length()<2 || nom_.length()>60){
            throw new BadPostCodeException("El nombre debe tener longitud de entre 2 y 60 caracteres");
        }        
    }
    public void validarDni(int dni_) throws BadPostCodeException{
        if(dni_ <= 0 && dni_> 999999999){
            throw new BadPostCodeException("DNI no valido");
        }
    }
}
